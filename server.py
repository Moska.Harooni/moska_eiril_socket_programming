from socket import create_server, socket
import random
import sys
import threading

PORT = 5050

HOST = 'localhost'
ADDRESS_FOR_AF_INET = (HOST, PORT)

print("Server listening on port", PORT)
clients ={}



#server_socket = create_server((HOST, PORT))#type of network 
#server_socket.listen()
#role =random.choice(ROLES)

def accept(server_socket): #for å godta connection
    while True:
        try: 
            conn, address = server_socket.accept()# Should be ready
            print(f"accept {conn} form {address}")
            thread = threading.Thread(target=handle_connection, args =(conn,address))
            thread.start()
        except KeyboardInterrupt:
            print("\nClosing server stocket...")
            server_socket.close()
            sys.exit()
    


def handle_connection(conn,address):
    while True:
        try:
            data = conn.recv(2024).decode() # Should be ready
            if data == "Role":
                role(conn)
            if data == "Situation":
                situation(conn)
                response = conn.recv(2024).decode()
                print(response)
            if data == "Advice":
                advice(conn)
                response = conn.recv(2024).decode()
                print(response)
        except ConnectionResetError:
            print(f"Client{address} disconnected")
            clients.pop(address)
            break



def role(conn):
    ROLES = ['advisee','advisor']
    role =random.choice(ROLES)
    print(f"Server role: {role}")
    conn.send(role.encode())

def advice(conn):
    advices = ['sit inside','get a towel']
    role =random.choice(advices)
    conn.send(role.encode())

def situation(conn):
    situations = ['it rains too much','i am wet']
    role =random.choice(situations)
    conn.send(role.encode())



def start():
    server_socket = create_server(ADDRESS_FOR_AF_INET)
    server_socket.listen()
    try: 
        while True:
            accept(server_socket)
    except KeyboardInterrupt:
        print("Server stopped")
        server_socket.close()


if __name__ == '__main__':
    start()


